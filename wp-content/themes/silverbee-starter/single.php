<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 07/05/2018
 * Time: 13:22
 */

get_header(); ?>

    <div class="breadcrumb-wrapper">
        <div class="row justify-content-center">
            <div class="col-11">
				<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
				?>
            </div>
        </div>
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <section class="page">
                <div class="page-content page-single-content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-md-12 col-lg-4 col-xl-3 order-2 order-lg-1">
                                <?php get_sidebar() ?>
                            </div>
                            <div class="col-md-12 col-lg-8 col-xl-8 order-1 order-lg-2">
                                <div class="nieuws-single-wrapper">
                                    <div class="row">
										<?php
										while ( have_posts() ) : the_post();

											get_template_part( 'template-parts/content', 'news' );

										endwhile; // End of the loop.
										?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();