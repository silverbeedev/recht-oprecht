<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

?>

</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="site-info">
        <div class="row justify-content-center">
            <div class="col-6 offset-sm-0 col-md-3 col-lg-2 offset-lg-1 offset-xl-0 align-self-center">
                <div class="footer-left">
					<?php the_custom_logo() ?>
<!--                    <div class="footer-social">-->
<!--                        <a href="https://www.linkedin.com/company/recht-oprecht-b.v./" target="_blank">-->
<!--                            <div class="icon li"></div>-->
<!--                        </a>-->
<!--                        <a href="https://www.facebook.com/rechtoprecht/" target="_blank">-->
<!--                            <div class="icon fb"></div>-->
<!--                        </a>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="col-12 col-md-8 col-lg-9 align-self-center">
                <div class="footer-menu">
					<?php wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'footer-menu',
						'menu_class'     => 'bottom-menu',
					) ); ?>
                </div>
            </div>
        </div>
    </div><!-- .site-info -->
</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
