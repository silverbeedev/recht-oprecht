<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 14/05/2018
 * Time: 12:56
 */ ?>

<?php
global $wp;
$url    = home_url( $wp->request ) . '/';
$parent = wp_get_post_parent_id( get_the_ID() );
if ( is_home() ) {
	$parent = wp_get_post_parent_id( get_option( 'page_for_posts' ) );
}
if ( ! $parent ) {
	$parent = get_the_ID();
}

if ( is_front_page() ) {
	$parent = 70;
}

$children = get_posts( array(
	'post_parent' => $parent,
	'post_type'   => 'page',
	'orderby'     => 'publish_date',
	'order'       => 'ASC',
	'numberposts' => '-1'
) );
?>

<?php if ( is_front_page() ) : ?>

    <div class="expertises-children-content">
        <div class="entry-header text-center">
            <h2 class="entry-title">
                <a href="<?php echo get_the_permalink( $parent ) ?>">
	                <?php echo get_the_title( $parent ); ?>
                </a>
            </h2>
        </div>
        <div class="expertises-child-wrapper">
			<?php if ( $children ): ?>
                <div class="row justify-content-center">
					<?php foreach ( $children as $child ) : ?>
                        <div class="col-sm-11 col-md-4 col-lg-4 col-xl-4">
                            <div class="expertises-child-item front">
                                <div class="expertise-icon">
                                    <a href="<?php the_permalink( $child ) ?>">
                                        <span><?php echo get_the_post_thumbnail( $child ); ?></span>
                                    </a>
                                </div>
                                <div class="expertises-title">
                                    <a href="<?php the_permalink( $child ) ?>">
                                        <h3><?php echo $child->post_title; ?></h3>
                                    </a>
                                </div>
                                <div class="expertises-desc">
									<?php echo $child->post_excerpt; ?>
                                    <div class="expertises-read-more text-right">
                                        <a href="<?php the_permalink( $child ) ?>">
                                            <div class="readmore-btn">
                                                <span>Lees meer</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php
					endforeach; ?>
                </div>
			<?php endif; ?>
        </div>
    </div>

<?php else : ?>

    <div class="expertises-children-content">
        <header class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>
        <div class="expertises-child-wrapper">
			<?php if ( $children ): ?>
                <div class="row">
					<?php foreach ( $children as $child ) : ?>
                        <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
                            <div class="expertises-child-item">
                                <div class="expertise-icon">
                                    <a href="<?php the_permalink( $child ) ?>">
                                        <span><?php echo get_the_post_thumbnail( $child ); ?></span>
                                    </a>
                                </div>
                                <div class="expertises-title">
                                    <a href="<?php the_permalink( $child ) ?>">
                                        <h3><?php echo $child->post_title; ?></h3>
                                    </a>
                                </div>
                            </div>
                        </div>
					<?php
					endforeach; ?>
                </div>
			<?php endif; ?>
        </div>
    </div>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="entry-content">

			<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'silverbee-starter' ),
				'after'  => '</div>',
			) );
			?>
        </div><!-- .entry-content -->

		<?php if ( get_edit_post_link() ) : ?>
            <footer class="entry-footer">
				<?php
				edit_post_link(
					sprintf(
						wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="screen-reader-text">%s</span>', 'silverbee-starter' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
				?>
            </footer><!-- .entry-footer -->
		<?php endif; ?>
    </article><!-- #post-<?php the_ID(); ?> -->

<?php endif; ?>