<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 24/04/2018
 * Time: 16:58
 */
?>

<!--DISPLAY BLOG ITEMS ON HOMEPAGE-->

<?php
wp_reset_query();
//if ( is_front_page() ) :

global $post;
$args    = array(
	'posts_per_page' => 1,
);
$myposts = get_posts( $args );

foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
    <div class="blog-item">
        <div class="row justify-content-center">
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="blog-item-thumb">
                    <img src="<?php the_post_thumbnail_url() ?>" alt="<?php get_the_post_thumbnail_caption() ?>">
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-xl-12">
                <h3 class="blog-item-title">
                    <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                </h3>
                <div class="blog-item-date"><?php the_date(); ?></div>
                <div class="blog-item-excerpt"><?php the_excerpt(); ?></div>
            </div>
        </div>
    </div>

<?php endforeach;
wp_reset_postdata();

//endif; ?>
