<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 14/05/2018
 * Time: 14:05
 */ ?>

<?php if ( is_front_page() ) : ?>

    <div class="review-wrapper">
<!--        <h2>--><?php //echo __('Reviews', 'silverbee-starter'); ?><!--</h2>-->
        <div class="row justify-content-center">
            <div class="col-11 col-sm-8 col-md-6 col-lg-4 align-self-center">
                <div class="progressbar" data-animate="false">
		            <?php echo do_shortcode( '[testimonials_count show_aggregate_rating="1"]' ); ?>
                    <div class="row inner">
                        <div class="col-xs-5">
                            <div class="circle">
                                <div></div>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="based_on">
                                <p>Gebaseerd op <span class="total"></span> reviews</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-11 col-sm-8 col-md-6 col-lg-5">
	            <?php echo do_shortcode( '[testimonials theme="no_style" paginate="0" count="1" orderby="date" order="DESC" show_title="1" use_excerpt="1" show_thumbs="0" show_date="1" show_other="0" hide_view_more="1" output_schema_markup="0" show_rating="stars"]' ) ?>
                <a href="/reviews">
                    <div class="more-reviews">
                        Bekijk alle reviews
                    </div>
                </a>
            </div>
        </div>
    </div>

<?php else : ?>

    <div class="review-wrapper">
        <h1><?php echo the_title(); ?></h1>
        <div class="average-rating-wrapper">
            <div class="row justify-content-center">
                <div class="progressbar" data-animate="false">
                    <div class="row">
                        <div class="col-xs-5">
                            <div class="circle">
                                <div></div>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="based_on">
                                <p>Gebaseerd op <span class="total"></span> reviews</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php
		echo do_shortcode( '[testimonials_count show_aggregate_rating="1"]' );
//		echo do_shortcode( '[testimonials theme="no_style" paginate="all" count="99" orderby="date" order="DESC" show_title="1" use_excerpt="0" show_thumbs="0" show_date="1" show_other="0" hide_view_more="1" output_schema_markup="0" show_rating="stars"]' );
        echo do_shortcode('[testimonials theme="no_style" paginate="max" testimonials_per_page="5" count="5" orderby="date" order="DESC" show_title="1" use_excerpt="0" show_thumbs="0" show_date="1" show_other="0" hide_view_more="1" output_schema_markup="1" show_rating="stars"]');
		?>
    </div>

<?php endif; ?>