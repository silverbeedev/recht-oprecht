<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 14/05/2018
 * Time: 13:51
 */ ?>

<div class="cta-wrapper">
	<?php get_template_part( 'template-parts/parts/part', 'cta' ); ?>
</div>

<div class="abonnement-content">
    <div class="row justify-content-center">
		<?php get_template_part( 'template-parts/parts/part', 'main-abonnement' ); ?>
    </div>
</div>