<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

?>
<!-- query -->
<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$query = new WP_Query( array(
	'category_name' => 'Blog',
	'slug' => 'blog',
	'posts_per_page' => 6,
	'paged' => $paged
) );
?>

<?php if ( $query->have_posts() ) : ?>

    <!-- begin loop -->
<div class="nieuws-wrapper">
    <div class="row">
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>

            <div class="col-12 col-sm-6 col-md-6 col-lg-4 margin">
                <div class="nieuws-item">
                    <div class="nieuws-item-thumbnail" style="background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(0,0,0, .65) 85%), url('<?php echo get_the_post_thumbnail_url(); ?>')">
                        <div class="nieuws-item-date">
							<?php echo get_the_date(); ?>
                        </div>
                        <div class="nieuws-item-title">
                            <a href="<?php echo get_the_permalink() ?>">
                                <h3>
									<?php echo get_the_title(); ?>
                                </h3>
                            </a>
                        </div>
                    </div>
                    <div class="nieuws-item-excerpt">
						<?php if ( has_excerpt() ) : ?>
                            <p>
								<?php echo get_the_excerpt(); ?>
                            </p>
						<?php else : ?>
                            <p>
                                Er is nog geen samenvatting ingesteld ...
                            </p>
						<?php endif; ?>
                    </div>
                    <div class="nieuws-item-readmore">
                        <a href="<?php echo get_the_permalink() ?>">
                            <div class="readmore-btn">
                                Lees meer
                            </div>
                        </a>
                    </div>
                </div>
            </div>

		<?php endwhile; ?>
        <!-- end loop -->
    </div>
    <div class="pagination">
		<?php
		echo paginate_links( array(
			'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			'total'        => $query->max_num_pages,
			'current'      => max( 1, get_query_var( 'paged' ) ),
			'format'       => '?paged=%#%',
			'show_all'     => false,
			'type'         => 'plain',
			'end_size'     => 2,
			'mid_size'     => 1,
			'prev_next'    => true,
			'add_args'     => false,
			'add_fragment' => '',
		) );
		?>
    </div>


	<?php wp_reset_postdata(); ?>
</div>

<?php else : ?>
    <p><?php _e( 'Helaas, er zijn geen nieuwsberichten om te laden ...' ); ?></p>
<?php endif; ?>
