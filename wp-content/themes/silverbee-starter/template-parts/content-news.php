<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 24/04/2018
 * Time: 16:58
 */

?>

<!--DISPLAY NEWS ITEMS ON HOMEPAGE-->

<?php
wp_reset_query();
if ( is_front_page() ) : ?>

	<?php
	global $post;
	$args    = array(
		'posts_per_page' => 3,
		'category_name'  => 'Blog'
	);
	$myposts = get_posts( $args );
	?>

<div class="nieuws-wrapper">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="entry-header text-center">
                <h2>
                    <a href="<?php echo get_the_permalink( get_option( 'page_for_posts', true ) );?>">
	                    <?php echo get_the_title( get_option( 'page_for_posts', true ) ); ?>
                    </a>
                </h2>
            </div>
        </div>
    </div>
    <div class="row">
	    <?php
	    foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="nieuws-item">
                <div class="nieuws-item-thumbnail" style="background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(0,0,0, .65) 85%), url('<?php echo get_the_post_thumbnail_url(); ?>')">
                    <div class="nieuws-item-date">
                        <?php echo get_the_date(); ?>
                    </div>
                    <div class="nieuws-item-title">
                        <a href="<?php echo get_the_permalink() ?>">
                            <h3>
		                        <?php echo get_the_title(); ?>
                            </h3>
                        </a>
                    </div>
                </div>
                <div class="nieuws-item-excerpt">
	                <?php if ( has_excerpt() ) : ?>
                    <p>
		                <?php echo get_the_excerpt(); ?>
                    </p>
                    <?php else : ?>
                    <p>
                        Er is nog geen samenvatting ingesteld ...
                    </p>
                    <?php endif; ?>
                </div>
                <div class="nieuws-item-readmore">
                    <a href="<?php echo get_the_permalink() ?>">
                        <div class="readmore-btn">
                            Lees meer
                        </div>
                    </a>
                </div>
            </div>
        </div>
	    <?php endforeach;
	    wp_reset_postdata(); ?>
    </div>
</div>

<?php
//	DISPLAY NEWS ITEMS ON HOMEPAGE

elseif ( is_home() ) : ?>
    <div class="col-md-6 col-lg-4">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="row justify-content-center">
                <div class="col-sm-11">
                    <div class="post-title">
						<?php
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						?>
                    </div>
                    <div class="post-excerpt">
                        <div class="post-thumbnail">
                            <a href="<?php echo get_permalink() ?>"><?php echo get_the_post_thumbnail(); ?></a>
                        </div>
                        <p>
							<?php the_excerpt(); ?>
                        </p>
                    </div>
                </div>
            </div>
        </article><!-- #post-<?php the_ID(); ?> -->
    </div>


<?php elseif ( is_single() ): ?>

    <div class="col-md-12">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="row justify-content-center">
                <div class="col-sm-12">
                    <div class="post-title">
						<?php
						the_title( '<h1 class="entry-title">', '</h1>' );
						?>
                    </div>
                    <div class="post-date">
                        Geplaatst op: <span><?php the_date(); ?></span>
                    </div>
                    <div class="post-thumbnail">
						<?php echo get_the_post_thumbnail(); ?>
                    </div>
                    <div class="post-content">
                        <div class="entry-content">
							<?php
							the_content( sprintf(
								wp_kses(
								/* translators: %s: Name of current post. */
									__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'silverbee-starter' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							) );
							?>
                        </div><!-- .entry-content -->
                    </div>
                </div>
            </div>
        </article><!-- #post-<?php the_ID(); ?> -->
    </div>
<?php endif; ?>


