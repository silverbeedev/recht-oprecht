<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 07/05/2018
 * Time: 16:49
 */

$abonnement_title = get_field( 'abonnement_title', 26 );
$abonnement_prijs = get_field( 'abonnement_prijs' ,26 );
//Subfield prijs: 'prijs'
//Subfield btw: 'prijs_btw'
$abonnement_list      = get_field( 'abonnement_list', 26 );
$abonnement_button    = get_field( 'abonnement_button', 26 );
$abonnement_beschrijving    = get_field( 'abonnement_beschrijving', 26 );


$abonnement_title_anders = get_field( 'abonnement_title_anders', 26 );
$abonnement_prijs_anders = get_field( 'abonnement_prijs_anders' ,26 );
//Subfield prijs: 'prijs'
//Subfield btw: 'prijs_btw'
$abonnement_list_anders      = get_field( 'abonnement_list_anders', 26 );
$abonnement_button_anders    = get_field( 'abonnement_button_anders', 26 );
$abonnement_beschrijving_anders    = get_field( 'abonnement_beschrijving_anders', 26 );

?>

<div class="row row-eq-height justify-content-center">
    <?php if ( is_front_page() ) : ?>
    <div class="<?php if (is_front_page() ) : ?> col-sm-12 col-md-6 col-lg-5 col-xl-4 <?php else : ?>col-md-12 col-lg-12 col-xl-12 <?php endif;?>">
        <div class="abonnement-item item item-left">
            <div class="left-wrapper">
                <div class="left-title"><?php echo $abonnement_title_anders ?></div>
            </div>
            <div class="left-desc">
                <p>
					<?php echo $abonnement_beschrijving_anders ?>
                </p>
            </div>
            <a href="/over-ons">
                <div class="item-cta">
                    <span><?php echo $abonnement_button_anders ?></span>
                </div>
            </a>
        </div>
    </div>
    <?php endif; ?>
	<div class="<?php if (is_front_page() ) : ?> col-sm-12 col-md-6 col-lg-5 col-xl-4 <?php else : ?>col-md-12 col-lg-12 col-xl-12 <?php endif;?>">
		<div class="abonnement-item item item-right">
			<div class="item-title">
                <span class="ic1"><?php echo $abonnement_title ?></span>
                <span class="ic2"><?php echo $abonnement_prijs['prijs'] ?></span>
			</div>
			<div class="item-desc">
				<p>
					<?php echo $abonnement_beschrijving ?>
				</p>
			</div>
			<a href="/abonnement">
				<div class="item-cta">
					<span><?php echo $abonnement_button ?></span>
				</div>
			</a>
		</div>
	</div>
</div>