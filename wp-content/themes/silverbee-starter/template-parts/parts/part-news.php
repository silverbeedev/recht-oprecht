<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 14/05/2018
 * Time: 13:54
 */

global $post;
$args    = array(
	'posts_per_page' => 3,
	'category_name'  => 'Nieuwsberichten'
);
$myposts = get_posts( $args );
?>
	<h3>Recent nieuws</h3>

<?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

	<div class="nieuws-item">
		<div class="row">
			<div class="col-md-12">
				<div class="nieuws-item-date"><?php the_date(); ?></div>
				<h4 class="nieuws-item-title">
					<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
				</h4>
			</div>
		</div>
	</div>

<?php endforeach;
wp_reset_postdata();