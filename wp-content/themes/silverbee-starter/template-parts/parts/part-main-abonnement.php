<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 24/07/2018
 * Time: 15:13
 */

// Variables Front-page //

$titel_left = get_field('titel_left');
$quote_left = get_field('quote_left');
$button_tekst_left = get_field('button_tekst_left');
$button_link_left = get_field('button_link_left');

$titel_right = get_field('titel_right');
$quote_right = get_field('quote_right');
$button_tekst_right = get_field('button_tekst_right');
$button_link_right = get_field('button_link_right');

// Abonnement overview //

$abonnement_title = get_field( 'abonnement_title', 26 );
$abonnement_prijs = get_field( 'abonnement_prijs', 26 );
//Subfield prijs: 'prijs'
//Subfield btw: 'prijs_btw'
$abonnement_list         = get_field( 'abonnement_list', 26 );
$abonnement_button       = get_field( 'abonnement_button', 26 );
$abonnement_beschrijving = get_field( 'abonnement_beschrijving', 26 );

?>

<?php if ( is_front_page() ) : ?>
    <div class="col-11 col-sm-11 col-md-10 col-lg-5 align-self-center">
        <div class="abonnement-item item-left">
            <div class="item-desc">
                <h3><?php echo $titel_left; ?></h3>
                <blockquote>
                    <?php echo $quote_left; ?>
                </blockquote>
                <a href="<?php echo $button_link_left; ?>">
                    <div class="readmore-btn">
		                <?php echo $button_tekst_left; ?>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="col-11 col-sm-11 col-md-10 col-lg-5 align-self-center">
        <div class="abonnement-item item-right">
            <div class="item-desc">
                <h3><?php echo $titel_right; ?></h3>
                <blockquote>
		            <?php echo $quote_right; ?>
                </blockquote>
                <a href="<?php echo $button_link_right; ?>">
                    <div class="readmore-btn">
			            <?php echo $button_tekst_right; ?>
                    </div>
                </a>
            </div>
        </div>
    </div>


<?php else : ?>

    <div class="col-12">
        <div class="abonnement-item">
            <div class="abonnement-title">
                <h3>
					<?php echo $abonnement_title; ?>
                </h3>
            </div>
            <div class="abonnement-prijs">
				<?php echo $abonnement_prijs['prijs']; ?>
            </div>
            <div class="abonnement-list">
                <ul>
					<?php
					if ( have_rows( 'abonnement_list', 26 ) ):
						while ( have_rows( 'abonnement_list', 26 ) ) : the_row(); ?>
                            <li>
                            <span>
                                <img src="<?php echo get_template_directory_uri() ?>/img/checked.svg" alt="">
                            </span>
								<?php the_sub_field( 'list_item' ); ?>
                            </li>
						<?php endwhile;
					else : ?>
					<?php endif; ?>
                </ul>
            </div>
            <div class="col-12">
                <a href="/contact">
                    <div class="cta-button">
                        Word direct abonnee
                    </div>
                </a>
            </div>
        </div>
    </div>

<?php endif; ?>