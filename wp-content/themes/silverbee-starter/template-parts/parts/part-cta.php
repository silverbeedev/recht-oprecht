<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 26/07/2018
 * Time: 10:29
 */ ?>

<div class="row justify-content-center">
    <div class="col-8">
        <div class="cta-image">
            <img src="<?php echo get_template_directory_uri() ?>/img/mike-cta.png" alt="">
        </div>
    </div>
    <div class="col-12 text-center">
        <div class="cta-title">
            <h2>
				<?php if ( is_page( 'expertises' ) || is_page( 'arbeidsrecht' ) || is_page( 'contractenrecht' ) || is_page( 'privacyrecht' ) ) :
					echo __( 'Heeft u juridisch advies nodig?', 'silverbee-starter' );
				else :
					echo __( 'Heeft u een vraag?', 'silverbee-starter' );
				endif;
				?></h2>
        </div>
    </div>
    <div class="col-12">
        <a href="/contact">
            <div class="cta-button">
                Neem contact op
            </div>
        </a>
    </div>
</div>
