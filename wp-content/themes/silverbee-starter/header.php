<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text"
       href="#content"><?php esc_html_e( 'Skip to content', 'silverbee-starter' ); ?></a>

    <header id="masthead"
            class="site-header <?php if ( ! is_front_page() && !is_page('bedankt') ) : ?>page-header<?php elseif (is_page('bedankt')) : ?>front-header bedankt-header<?php else : ?>front-header<?php endif; ?>"
            role="banner">
        <div class="container-fluid" id="header-wrapper">
            <div class="navbar-overlay">
                <div class="navbar-wrapper">
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-11 nav-wrapper">
                            <nav class="navbar navbar-dark">
                                <div class="navbar-brand">
							        <?php the_custom_logo(); ?>
                                </div>
                                <button class="navbar-toggler" type="button" data-toggle="collapse"
                                        data-target="#navbar7">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </nav>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-11 text-center">
                            <div class="navbar-overlay-search">
						        <?php get_search_form() ?>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-12 col-lg-11 text-center">
                            <div class="navbar-overlay-list">
						        <?php wp_nav_menu( array(
							        'theme_location' => 'menu-1',
							        'menu_id'        => 'primary-menu',
							        'menu_class'     => 'top-menu',
						        ) ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-4 text-center">
                            <div class="navbar-overlay-social">
                                <a href="https://www.linkedin.com/company/recht-oprecht-b.v./" target="_blank">
                                    <div class="icon li"></div>
                                </a>
                                <a href="https://www.facebook.com/rechtoprecht/" target="_blank">
                                    <div class="icon fb"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php if ( is_front_page() || is_page('bedankt') ) : ?>
            <div class="telefoon-wrapper">
                <a href="tel:0618255032">
                    <span class="tel">06-18255032</span>
                </a>
            </div>
            <div class="navbar-wrapper front">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-10 nav-wrapper">
                        <nav class="navbar navbar-dark">
                            <div class="navbar-brand">
								<?php the_custom_logo(); ?>
                            </div>
                            <button class="navbar-toggler non-overlay" type="button" data-toggle="collapse" data-target="#navbar7">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="post-nav-wrapper">
                <div class="row">
                    <div class="col-sm-11 col-md-6 offset-md-0 col-lg-5 col-xl-4 offset-lg-1 align-self-center">
                        <div class="site-description-wrapper">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?php else : ?>
            <div class="telefoon-wrapper t-page">
                <a href="tel:0618255032">
                    <span class="tel">06-18255032</span>
                </a>
            </div>
            <div class="navbar-wrapper navbar-wrapper-page">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-11 nav-wrapper">
                        <nav class="navbar navbar-dark">
                            <div href="/" class="navbar-brand">
								<?php the_custom_logo(); ?>
                            </div>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar7">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </nav>
                    </div>
                </div>
            </div>
		<?php endif; ?>
    </header><!-- #masthead -->

    <div id="content" class="site-content">
