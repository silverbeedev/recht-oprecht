<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 07/05/2018
 * Time: 13:22
 */

get_header(); ?>

    <div class="breadcrumb-wrapper">
        <div class="row justify-content-center">
            <div class="col-11">
				<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
				?>
            </div>
        </div>
    </div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="page">
				<div class="page-content page-nieuwsarchive-content">
					<div class="container-fluid">
						<div class="row justify-content-center">
							<div class="col-md-10">
								<div class="nieuws-archive-wrapper">
									<h1 class="text-center">Nieuws</h1>
									<div class="row">
										<?php


											get_template_part( 'template-parts/content', 'newsarchive' );

                                        ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();