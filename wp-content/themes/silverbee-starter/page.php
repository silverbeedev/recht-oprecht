<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 07/05/2018
 * Time: 13:22
 */

get_header(); ?>

    <div class="breadcrumb-wrapper">
		<div class="row justify-content-center">
            <div class="col-11">
	            <?php
	            if ( function_exists( 'yoast_breadcrumb' ) ) {
		            yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
	            }
	            ?>
            </div>
        </div>
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <section class="page">
                <div class="page-content page-all custom">
                    <div class="container-fluid">
                        <div id="page-content">
                            <div class="row justify-content-center">
                                <div class="col-md-12 col-lg-5 col-xl-3 order-2 order-lg-1">
                                    <div class="content-left">
	                                    <?php get_template_part( 'template-parts/content', 'leftside' ); ?>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-7 col-xl-8 order-1 order-lg-2">
                                    <div class="content-right">
										<?php
										while ( have_posts() ) : the_post();
										    if ( is_page(70) ) :
                                                get_template_part( 'template-parts/content', 'expertises' );
										    elseif ( is_page(29) ) :
                                                get_template_part( 'template-parts/content', 'reviews' );
										    else :
                                                get_template_part( 'template-parts/content', 'page' );
										    endif;
										endwhile; // End of the loop.
										?>

<!--	                                    --><?php
//
//                                        if ( is_page(89) || is_page(91) || is_page(93) ) :
//
//	                                    $pagelist = get_pages("child_of=".$post->post_parent."&parent=".$post->post_parent."&sort_column=menu_order&sort_order=asc");
//	                                    $pages = array();
//	                                    foreach ($pagelist as $page) {
//		                                    $pages[] += $page->ID;
//	                                    }
//
//	                                    $current = array_search($post->ID, $pages);
//	                                    $prevID = $pages[$current-1];
//	                                    $nextID = $pages[$current+1];
//	                                    ?>
<!---->
<!--                                        <div class="navigation">-->
<!--                                            <div class="pre-navigation">-->
<!--                                                Andere expertises:-->
<!--                                            </div>-->
<!--		                                    <div class="row">-->
<!--                                                <div class="col-12">-->
<!--	                                                --><?php //if (!empty($prevID)) { ?>
<!--                                                        <div class="previous">-->
<!--                                                            <a href="--><?php //echo get_permalink($prevID); ?><!--" title="--><?php //echo get_the_title($prevID); ?><!--">-->
<!--				                                                --><?php //echo get_the_title($prevID); ?>
<!--                                                            </a>-->
<!---->
<!--                                                        </div>-->
<!--	                                                --><?php //}
//	                                                if (!empty($nextID)) { ?>
<!--                                                        <div class="next">-->
<!--                                                            <a href="--><?php //echo get_permalink($nextID); ?><!--" title="--><?php //echo get_the_title($nextID); ?><!--">-->
<!--				                                                --><?php //echo get_the_title($nextID); ?>
<!--                                                            </a>-->
<!--                                                        </div>-->
<!--	                                                --><?php //} ?>
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        --><?php //endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();