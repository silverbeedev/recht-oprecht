<?php
/**
 * The template for displaying the front-page
 *
 * @package Silverbee_Starter
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="front-main site-main" role="main">
            <section class="front-page">
                <div class="page-content">
                    <div id="post-header-wrapper">
                        <div class="expertises-wrapper">
                            <div class="row justify-content-center">
                                <div class="col-sm-12 col-md-11 col-lg-10">
									<?php get_template_part( 'template-parts/content', 'expertises' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="main-info-wrapper">
                            <div class="row row-eq-height ">
								<?php
								$id                 = 32;
								$post_over_ons      = get_post( $id ); // GET ID OF "OVER ONS" PAGE
								$excerpt_over_ons   = $post_over_ons->post_excerpt;
								$permalink_over_ons = get_post_permalink( $id );
								$thumbnail          = get_the_post_thumbnail_url( $id );
								?>

                                <div class="col-12 col-sm-8 offset-0 col-md-7 offset-md-1 col-lg-6 col-xl-5 align-self-center">
                                    <div class="over-ons-wrapper">
                                        <div class="over-ons-item">
                                            <h2>
                                                <a href="<?php echo get_the_permalink($id) ?>" class="link-white">
	                                                <?php echo get_the_title( $id ); ?>
                                                </a>
                                            </h2>
                                            <p>
												<?php echo $excerpt_over_ons; ?>
                                            </p>
                                            <div class="read-more">
                                                <a href="<?php echo $permalink_over_ons ?>">
                                                    <?php echo __('Lees meer over Mike Krak', 'silverbee-starter'); ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-abonnement-wrapper">
                            <div class="row justify-content-center">
                                <div class="col-10 text-center">
                                    <?php $titel_quote = get_field('titel_quote'); ?>
                                    <h2>
                                        <a href="/abonnement">
	                                        <?php echo $titel_quote; ?>
                                        </a>
                                    </h2>
                                </div>
                            </div>
                            <div class="abonnement-content">
                                <div class="row justify-content-center">
									<?php get_template_part( 'template-parts/parts/part', 'main-abonnement' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="pre-footer-reviews">
                            <div class="row row-eq-height justify-content-center">
                                <div class="col-md-11 col-lg-10 col-xl-10">
									<?php get_template_part( 'template-parts/content', 'reviews' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="pre-footer-news">
                            <div class="row justify-content-center">
                                <div class="col-lg-10 col-xl-10">
									<?php get_template_part( 'template-parts/content', 'news' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </div><!-- .page-content -->
    </section><!-- .front-page -->

    </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
