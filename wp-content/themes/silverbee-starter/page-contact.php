<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 07/05/2018
 * Time: 13:22
 */

get_header(); ?>

    <div class="breadcrumb-wrapper">
        <div class="row justify-content-center">
            <div class="col-11">
				<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
				?>
            </div>
        </div>
    </div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section class="page">
				<div class="page-content page-contact-content">
					<div class="container-fluid">
						<div class="row justify-content-center">
							<div class="col-md-7">
								<div class="contact-left">
									<?php
									while ( have_posts() ) : the_post();

										get_template_part( 'template-parts/content', 'page' );

									endwhile; // End of the loop.
									?>
								</div>
							</div>
                            <div class="col-4">
                                <div class="contact-img">
                                </div>
                            </div>
						</div>
                        <div class="map-wrapper">
                            <div class="row justify-content-center">
                                <div class="col-md-11">
                                    <div class="contact-right">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-12">
                                                <div class="contact-maps">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2487.682732266193!2d5.4386406516892505!3d51.42725447952207!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6d98315f2fb3f%3A0xd7f5ae310a3a1f19!2sMeerenakkerplein+51%2C+5652+BJ+Eindhoven!5e0!3m2!1sen!2snl!4v1525693201378" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                </div>
                                                <div class="contact-info-wrapper">
                                                    <div class="contact-info-title">
                                                        Het Kantoor
                                                    </div>
                                                    <div>
                                                        <strong>Recht-oprecht B.V.</strong>
                                                    </div>
                                                    <div class="contact-info-desc">
                                                        <p>Meerenakkerplein 51</p>
                                                        <p>5652 BJ Eindhoven</p>
                                                        <p><span>E</span> <a href="mailto:info@recht-oprecht.nl">info@recht-oprecht.nl</a></p>
                                                        <p><span>T</span> <a href="tel:06 18 25 50 32">06 18 25 50 32</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>

			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();