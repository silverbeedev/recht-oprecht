<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="side-nav" class="widget-area" role="complementary">
    <nav>
        <div id="site-navigation" class="main-navigation" role="navigation">
			<?php if ( is_single() ) : ?>
            <div class="nieuws-nav">
                <h2>Meer nieuws</h2>
	            <?php get_sidebar( 'news' ); ?>
            </div>
			<?php else : ?>
				<?php get_sidebar( 'children' ); ?>
			<?php endif; ?>
        </div>
    </nav><!-- #site-navigation -->
</aside><!-- #secondary -->
