(function ($) {
    $get_average_rating = $('.easy_t_aggregate_rating_top_count').text();
    $get_total_rating = $('.easy_t_aggregate_rating_review_count').text();

    $calculation_rating = (($get_average_rating / 5) * 10);
    $grade = Number($calculation_rating).toFixed(1);

    $('.progress-value').html($grade);
    $('.total').html($get_total_rating);

    function animateElements() {
        $('.progressbar').each(function () {
            var elementPos = $(this).offset().top;
            var topOfWindow = $(window).scrollTop();
            var percent = $grade; //$(this).find('.circle').attr('data-percent');
            var percentage = parseInt(percent, 10) / parseInt(100, 10);
            var animate = $(this).data('animate');
            if (elementPos < topOfWindow + $(window).height() - 30 && !animate) {
                $(this).data('animate', true);
                $(this).find('.circle').circleProgress({
                    startAngle: -Math.PI / 2,
                    value: percent / 10,
                    size: 130,
                    thickness: 15,
                    foregroundBorderWidth: 0,
                    backgroundBorderWidth: 0,
                    emptyFill: "rgba(0,0,0, .05)",
                    fill: {
                        color: '#74B9E8'

                    }
                }).on('circle-animation-progress', function (event, progress, stepValue) {
                    $(this).find('div').text((stepValue * 10).toFixed(1) + "");
                }).stop();
            }
        });
    }

    $(document).ready(function () {

        animateElements();
        $(window).scroll(animateElements);

        var navbar_toggler = $('button.navbar-toggler');

        $i = 0;

        navbar_toggler.click(function (e) {
            $i++;
            if ($i % 2 === 0) {
                $(".navbar-overlay > .navbar-wrapper").fadeOut(500, function () {
                    $('body').css('overflow-y', 'auto');
                    $(".navbar-overlay").animate({width: '0'});
                });

            }
            else {
                $(".navbar-overlay").animate({width: '100%'}, function () {
                    $('body').css('overflow-y', 'hidden');
                    $(".navbar-overlay > .navbar-wrapper").fadeIn(500);
                });
            }
        });
    });
})(jQuery);