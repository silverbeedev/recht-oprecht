<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 07/05/2018
 * Time: 13:22
 */

get_header();

//Get custom fields
$abonnement_title = get_field( 'abonnement_title' );
$abonnement_prijs = get_field( 'abonnement_prijs' );
//Subfield prijs: 'prijs'
//Subfield btw: 'prijs_btw'
$abonnement_list      = get_field( 'abonnement_list' );
$abonnement_uurtarief = get_field( 'abonnement_uurtarief' );
$abonnement_button    = get_field( 'abonnement_button' );

?>

    <div class="breadcrumb-wrapper">
        <div class="row justify-content-center">
            <div class="col-11">
				<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
				?>
            </div>
        </div>
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <section class="page page-all">
                <div class="page-content page-abonnement-content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-lg-3 col-xl-3">
                                <div class="content-left">
                                    <div class="abonnement-overview">
                                        <div class="row row-eq-height">
                                            <div class="col-sm-12 mx-auto col-lg-12">
                                                <div class="abonnement-content">
                                                    <div class="row justify-content-center">
			                                            <?php get_template_part( 'template-parts/parts/part', 'main-abonnement' ); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-8">
                                <div class="content-right">
                                    <div class="content-wrapper">
										<?php
										while ( have_posts() ) : the_post();

											get_template_part( 'template-parts/content', 'page' );

										endwhile; // End of the loop.
										?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();