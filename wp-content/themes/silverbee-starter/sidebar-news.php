<?php
$url    = home_url( $wp->request ) . '/';
$args = array(
	'numberposts' => 5,
	'orderby' => 'post_date',
	'order' => 'DESC',
	'meta_value' =>'',
	'post_type' => 'post',
);
$recent_posts = wp_get_recent_posts( $args);

?>
	<ul>
		<?php
		foreach( $recent_posts as $recent ){
			$post_active = get_permalink($recent["ID"]);
			if ($url === $post_active){
				echo '<li class="active"><a href="' . get_permalink($recent["ID"]) . '">' .   ( __($recent["post_title"])).'</a> </li> ';
			}
			else{
				echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   ( __($recent["post_title"])).'</a> </li> ';
			}
		}
		?>
	</ul>
<?php
wp_reset_query();
?>