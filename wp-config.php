<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
require __DIR__ . '/wp-content/vendor/autoload.php';


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'recht-oprecht' );

/** MySQL database username */
define( 'DB_USER', 'homestead' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'WP_CONTENT_DIR', __DIR__ . '/wp-content' );

define( 'WP_HOME', 'http://recht-oprecht.test' );

define( 'WP_SITEURL', 'http://recht-oprecht.test' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'KnXvCz?ec#O]>@h4Nnj`49+ObPNNb:rr4O}P8HY hnwK~]5{YCC5*WP{||6l6G=x');
define('SECURE_AUTH_KEY',  'Y^7jC3u@k7jz:Y/&lPF$Vl|Uh1I7.8K/b.3kZNBBJ s<EvWq_T+t`R(.IZeZ_6?=');
define('LOGGED_IN_KEY',    'PWA1W%fPf`o+`8o@%F)^pySS@0q$OaEt`35GH=oC&W6[|1,ZbAkV^_7T*THA,_-]');
define('NONCE_KEY',        ',B.2ano(>?7n*<xTe7S|^<i-{F-1P0T@WWzd+5 dmtkxS~e89a};]Ytr-@$q70@#');
define('AUTH_SALT',        '~@4VG-i;6XI[p@a;r2]:fL]0^++QJ3oiHaIl2vGPV~#4yJb866BXjd~,gA&PF+{c');
define('SECURE_AUTH_SALT', 'HF9z&kqWLVvT0U$+bt>8h2+5Y@:>/$)ddg%7{--t{|bE0T-Sv-d(g~/QU^TEm{Y&');
define('LOGGED_IN_SALT',   '`il9 6H7jDh2w#+<9-@ub a?TI{PV<iB+-OurXe]+-3Fx!BX2y?)w^6Ufj(|@nrk');
define('NONCE_SALT',       'n[bIyqaaX=pOwT 8H:A/|Ml.<Kt!;F4^Vq|v&-IOz#_4rz0_KCO6wI<EOv$OPx~m');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
